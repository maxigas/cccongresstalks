% Interview questions
% CCCongresstalks research
% https://ccc.tachanka.org/

0. Explain https://ccc.tachanka.org/, maybe also
https://ccc.tachanka.org/synon (same but without unifying synonyms).

1. Almost every year includes introductory talks for beginners. Is this
about education? What is the role of this kind of talk?

2. Security-related terms popped up from the beginning, but in the last
few years it is clear that “security” as a keyword dominates the program.
How has the discussion about security changed through the years?  How
the industry context changed?

3. Terms like “Feminine computing” and “Haecksen” and gendered-language
have occurred early on the data set.  Will you please reflect upon how
Congress responds to gender?  Did the relationship with political
movements like feminisms changed over the years?

4. Do you think that Congress talks reflect history? Do they talk about
the past, present, or the future?  How the initiative of hackers shifted
during these three decades?

Say thanks and ask for contact if not already recorded, and mention that
we will be in touch about research milestones in case they are interested.
