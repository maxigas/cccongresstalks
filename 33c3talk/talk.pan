# Intro

The proper relationship of technology and politics have been the subject of an evergreen debate on the floor of the Chaos Communication Congress. Rather than taking a position in this debate, we are asking how the two have been co-articulated in practice so far by CCC participants?

The proper relationship of technology and politics and thereby the percentage each covers in the Congress schedule have been the subject of an evergreen debate at the floor and in the corridors of the Chaos Communication Congress. Rather than taking a position in this debate, we are asking how the two have been co-articulated in talks so far by CCC participants? In order to answer this question, we are analysing the available titles and abstracts of Congress talks from 1984 until now. This ongoing research seeks to identify changing trends, significant outliers, apparent patterns and common threads throughout the years. We also wonder if it is possible to identify turning points in the narrative. The empirical data is contextualised by reflections on the shifting ground of technology, politics and society in the world during the long history of the CCC, as well as by qualitative reflections of attendants. We are inviting the audience to help us with the latter by joining in a follow-up discussion after the presentation.

 * social-context-impact: unification, NSA --> show chart

# what did we find so far: a few shots

## 

 * ratio of type of talks
 * ratio and number throughout the years
 * word cloud
 * tech vs politics

## 

 * hand-raising about how many audience members were at how old congresses
 * repeated mention of a congress paper: where is it archived?
 * 1989: electronic newspaper mentioned again

 * 

# where do we want to go from here

## 

* we know that the tagging is subjective to some extend, but it's based on a methodology and we try to make everything as transparent as possible. comments are always welcome.
* what we are looking for now is people to talk with, counting the talks was a first and possibly the easiest step
* invite everyone to the workshop day 3 4pm room A.2

