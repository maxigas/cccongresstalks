------------------------------------------------------------------------------
Zeit | Aula             | Workshop-Raum 1   | Workshop-Raum 2   | Sonstiges
-----+------------------+-------------------+-------------------+-------------
08   |                  |                   |                   | Beginn
     |                  |                   |                   | Einlass und
     |                  |                   |                   | Kartenausg.
09   |                  |                   |                   |
     |                  |                   |                   |
     |                  |                   |                   |
10   | Begruessung und  |                   |                   |
     | Orienterung      |                   |                   |
     |                  |                   |                   |
     |                  |                   |                   |
11   | CCC vor Ort      | TCP/IP fuer       | Organisations-    |
     | Erfa-Kreise      | Anfaenger         | workshop          |
     | berichten ueber  |                   |                   |
     | Projekte,        |                   |                   |
12   | Aktionen und     |                   |                   |
     | zukuenftige      |                   |                   |
     | Koordination     |                   |                   |
     |                  |                   |                   |
13   |                  | Chaosradio -      | CCC Sachsen und   |
     |                  | der CCC als       | die Idee der      |
     |                  | Medienproduzent   | Politikseminare   |
     |                  |                   |                   |
14   |                  |                   |                   | Essen
     |                  |                   |                   |
     |                  |                   |                   |
     |                  |                   |                   |
15   | Zensur-Debatte   | Trust-Center I:   | Wie funktioniert  |
     |                  | Was ist und wer   | das Internet in   |
     |                  | traut einem       | den USA ?         |
     |                  | Trustcenter?      |                   |
16   |                  |                   | Vernetzung selbst-|
     |                  |                   | gemacht: Beispiel |
     |                  |                   | Prenzlnet         |
17   | Vorstellung d.   | THC++             |                   |
     | OnlineMagnaCharta|                   |                   |
     |                  |                   |                   |
18   | Wirtschafts-     |       -           |         -         |
     | spionage am      |                   |                   |
     | Beispiel PROMIS  |                   |                   |
19   | Film+Diskussion  |       -           |         -         |
     |                  |                   |                   |
19:30| ORGA-Hinweise    |                   |                   |
     | + Schlafplatz-   |                   |                   |
     | vergabe          |                   |                   |
     |                  |                   |                   |
20   | Chronik 1996:    |       -           |         -         |
     | CCC-Geschichte   |                   |                   |
     | und drumherum    |                   |                   |
21   | im Jahre -4      |       -           |         -         |
     |                  |                   |                   |
     |                  |                   |                   |
22   |                  |       -           |         -         | Schliessung

Tagesmenü:
	Grünkohl mit Kassler oder Kohlwurst
	Vegetarisch: Tortellini in Sahnesauce
	Täglich zwischendurch: Malayische Frikadellen mit Kokosnuss+Curry

