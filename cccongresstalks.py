#!/usr/bin/python3
# Download Chaos Communication Congress talk information and save in CSV file
import csv, os, codecs, re
import vobject, chardet

from bs4 import BeautifulSoup as bs
from collections import OrderedDict
from itertools import chain
from requests import get
from glob import glob

# -= Some basic global variables =-

# What we want:
fieldnames = ["year", "congress", "title", "abstract", "link", "tags"]
o = "cccongresstalks.csv"

# 1984-2015:
manualyears, htmlyears, icalyears = range(1984,1985), range(1985,2002), range(2002,2006)
# 1992 -> scanned pdfs without OCR at best or reviews
# 2002 -> machine readable files
xmlyears = range(2006,2020)

# First year, last serial number
y,s = 1984, 36

# Dictionary of year => congress abbreviation, e.g. 2016 => "33C3"
congresses = {i:str(x)+"C3" for i,x in zip(range(y,y+s), range(1, s+1))}

# -= CSV functions =-

def _writer(csvfile):
    return csv.DictWriter(csvfile,
                            fieldnames=fieldnames,
                            delimiter='|',
                            quoting=csv.QUOTE_ALL)

def _reader(csvfile):
    return csv.DictReader(csvfile,
                          fieldnames=fieldnames,
                          delimiter='|',
                          quoting=csv.QUOTE_ALL)

def save(filename, rows):
    with open(filename, 'a') as csvfile:
        writer = _writer(csvfile)
        for row in rows:
            # print(row)
            writer.writerow(row)

def new(filename):
    with open(filename, 'w') as csvfile:
        writer = _writer(csvfile)
        writer.writeheader()

def load(filename):
    rows = []
    with open(filename, 'r') as csvfile:
        reader = _reader(csvfile)
        for row in reader:
            rows.append(row)
    return rows[1:]

# -= iCal functions =-

def download(url, filename):
    print("Downloading: " + url)
    r = get(url)
    if r.status_code == 200:
        with open(filename, "wb") as f:
            f.write(r.content)
        print("Wrote " + filename)
        return True
    else:
        return False

def get_ical(year):
    urlhead = "https://events.ccc.de/congress/"
    names = ["schedule.ics", "schedule.en.ics", "fahrplan.ics"]
    names = ["/Fahrplan/" + x for x in names]
    names += [x.lower() for x in names]
    fail = True
    for name in names:
        if fail:
            url = urlhead + str(year) + name
            if download(url, str(year) + ".ics"):
                print("   SUCCESS:", url)
                fail = False
            else:
                print("   FAIL:", url)
                pass
    # Should be last resort because sometimes exists but empty:
    # http://www.ccc.de/congress/2002/19C3.ics
    if fail:
        url = urlhead+str(year)+"/"+congresses[year]+".ics"
        if download(url, str(year) + ".ics"):
            print("   SUCCESS:", url)
            fail = False
        else:
            print("   FAIL:", url)
            pass
    if not fail:
        print("* ", str(year), " OK")
        return True
    else:
        print("- ", str(year), " MISSING")
        return False

def slurp(filename):
    print("Slurping " + filename)
    with open(filename, 'rb') as f:
        raw = f.read()
    encoding = chardet.detect(raw)['encoding']
    with codecs.open(filename, 'r', encoding=encoding) as f:
        return f.readlines()

def fixlines(badlines):
    previous, goodlines = "", []
    # print("Received " + str(len(badlines)) + " badlines")
    for line in badlines:
        if "ATTENDEE" in line:
            pass
        elif (line.split(":")[0].isupper() or line.split(";")[0].isupper()) and (":" in line or ";" in line):
            goodlines.append(line)
        else:
            goodlines[-1] = goodlines[-1].strip() + line
    return goodlines

def single_line_abstracts(lines):
    out, description, indescription = [], "", False
    for l in lines:
        if ("CATEGORY" or "CLASS") in l:
            if description: out.append(description.replace(',',';')); # print("ADDED DESCRIPTION: " + description)
            description = ""
            indescription = False
            out.append(l)
        elif indescription:
            description += l.strip()
        elif "DESCRIPTION" in l:
            indescription = True
            description += l.strip()
        else:
            out.append(l)
    return out

def fix_misencodings(lines):
    out = []
    for l in lines:
        out.append(l.replace('Ăź','ß').replace('Ă¤','ä'))
    return out

def vobjectstreams(lines):
    """Accepts a list of lines, yields a list of vobjects streams"""
    # print("Received " + str(len(lines)) + " lines")
    vobjectstreams, buffer, inside = [], [], False
    for l in lines:
        if "BEGIN:VEVENT" in l:
            inside = True
            buffer.append(l)
        elif "END:VEVENT" in l:
            inside = False
            buffer.append(l)
            vobjectstream = ""
            for x in buffer:
                vobjectstream = vobjectstream + x
            vobjectstreams.append(vobjectstream)
            buffer = []
        elif inside:
            buffer.append(l)
    return vobjectstreams

def vobjects(vobjectstreams):
    """Accepts a list of vobject streams, yields a list of vobjects"""
    # print("Received " + str(len(vobjectstreams)) + " vobjectstreams")
    vs = []
    for v in vobjectstreams:
        try:
            vs.append(list(vobject.readComponents(v))[0])
        except:
            print("ERROR PARSING vobject:")
            print(v)
    return vs

def talks(vobjects):
    vs = vobjects
    # print("Received " + str(len(vobjects)) + " vobjects")
    year = int(vs[0].url.value.split("/")[4])
    congress = congresses[year]
    titles = [v.summary.value for v in vs]
    abstracts = [v.description.value if hasattr(v, 'description') else "N/F" for v in vs]
    abstracts = [re.sub(' +', ' ', a.replace('\n', ' ').strip() ) for a in abstracts]
    links = [v.url.value for v in vs]
    talks = []
    for t, a, l in zip(titles, abstracts, links):
        talks.append({"year": year,
                      "congress": congress,
                      "title": t,
                      "abstract": a,
                      "link": l,
                      "tags": ""})
    return talks

def ical(year):
    i = str(year) + ".ics"
    o = str(year) + ".csv"
    new(o)
    if os.path.isfile(i):
        print("Found existing " + i)
    else:
        print("Downloading " + i)
        get_ical(year)
    rows = talks(vobjects(vobjectstreams(fix_misencodings(single_line_abstracts(fixlines(slurp(i)))))))
    save(o, rows)
    return rows

# -= XML functions =-

def xml(year):
    link = "https://events.ccc.de/congress/" + str(year) + "/Fahrplan/schedule.xml" if year > 2012 else "https://events.ccc.de/congress/" + str(year) + "/Fahrplan/schedule.en.xml"
    rows, o, i = [], str(year) + ".csv", str(year) + ".xml"
    if os.path.isfile(o): print("Found " + o + ", skipping...!"); return True
    if os.path.isfile(i):
        print("Found " + i + ", parsing...!")
    else: download(link, i)
    xml = ""
    for line in slurp(i):
        xml += line
    soup = bs(xml, 'lxml')
    titles = [x.text + ": " + y.text if y.text else x.text for x,y in zip(soup.select('title'), soup.select('subtitle'))][1:]
    abstracts = [x.text.replace('\n',' ').replace('\r','') for x in soup.select('abstract')]
    new(o)
    for t, a in zip(titles, abstracts):
        print(str(year) + ": " + t)
        rows.append({'year': year,
                     'congress': congresses[year],
                     'title': t,
                     'abstract': a,
                     'link': link,
                     'tags': ""})
    save(o, rows)
    return rows

# -= HTML functions =-

def html(year):
    o = str(year) + ".csv"
    if os.path.isfile(o):
        print("Found " + o + ", skipping...!")
    else:
        handler = "html" + str(year)
        if handler in globals().keys():
            new(o)
            save(o, globals()[handler](year))
        else:
            print("HTML handler for year " + str(year) + " is missing!")

def html1993(year):
    year, rows, titles = 1993, [], []
    # Original gives "permission denied"
    link = "https://web.archive.org/web/20060521101447/http://events.ccc.de/congress/1993/"
    soup = bs(get(link).text, 'html5lib')
    for line in [x for x in soup.select('pre')[1].text.split('\n') if len(x) > 2]:
        # Stop processing after the first '-----', careful there is also '--' before!
        if line[2] == '-':
            break
        # titles' first char is '-' or '*'
        if line[0] == '-' or line[0] == '*':
            titles.append(line.strip('- ').strip('* ').strip())
    # Drop first two matches -- they are not talks:
    for t in titles[2:]:
        print(str(year) + ": " + t)
        rows.append({'year': year,
                     'congress': congresses[year],
                     'title': t,
                     'abstract': "N/A",
                     'link': link,
                     'tags': ""})
    return rows

def html1994(year):
    link = "https://events.ccc.de/congress/1994/"
    soup = bs(get(link).text, 'html5lib')
    links = [x['href'] for x in soup.select('dt a')]
    rows = []
    for l in links:
        soup = bs(get(link + l).text, 'html5lib')
        try:
            title = soup.select('h1')[0].text.replace('\n', ' ').strip()
        except IndexError:
            title = soup.select('h3')[0].text.replace('\n', ' ').strip()
        try:
            subtitle = soup.select('i')[0].text.replace('\n', ' ').strip()
        except IndexError:
            subtitle = ""
        title = title + ": " + subtitle if subtitle is not "" else title
        if "::" in title:
            title = title.replace("::", ":")
        print(str(year) + ": " + title)
        lines = [x.text.replace('\n', ' ').strip() for x in soup.select('p')]
        abstract = ""
        for line in lines:
            abstract = abstract + line.strip() + " "
        abstract = re.sub(' +', ' ', abstract.strip())
        rows.append({'year': year,
                     'congress': congresses[year],
                     'title': title,
                     'abstract': abstract,
                     'link': link,
                     'tags': ""})
    return rows

def html1995(year):
    year, rows, titles = 1995, [], []
    link = "https://events.ccc.de/congress/1995/fahrplan.htm"
    soup = bs(get(link).text, 'html5lib')
    maybetitles = [x.text.replace('\n', '').strip() for x in soup.select('td')]
    maybetitles = [x for x in maybetitles if len(x) > 5]
    stopwords = ['Tag', 'test', 'Beginn', 'Einlaß', 'Hinweise']
    for maybetitle in maybetitles:
        istitle = True
        for stopword in stopwords:
            if stopword in maybetitle:
                istitle = False
        if istitle:
            titles.append(maybetitle)
    for title in titles:
        print("1995: " + title)
    for title in titles:
        rows.append({'year': year,
                     'congress': congresses[year],
                     'title': title,
                     'abstract': "N/A",
                     'link': link,
                     'tags': ""})
    return rows

def html1996(year):
    currentday, year, rows, rooms = 0, 1996, [], {'Aula':[], 'Raum1':[], 'Raum2':[]}
    link = "https://events.ccc.de/congress/1996/Fahrplan.html"
    soup = bs(get(link).text, 'html5lib')
    days = [x.text for x in soup.findAll('pre')]
    for day in days:
        currentday += 1
        with open("1996day" + str(currentday) + ".txt", 'w') as f:
            f.write(day)
        lines = day.split('\n')
        inside = False
        for line in lines:
            if line[0] + line[1] == '22':
                break
            if line[0] + line[1] == '08':
                inside = True
            if inside:
                cols = line.split('|')
                rooms['Aula'].append(cols[1].strip())
                rooms['Raum1'].append(cols[2].strip())
                rooms['Raum2'].append(cols[3].strip())
    previous, titles = '', []
    for room in rooms.values():
        for line in room:
            # First line of a new title
            if previous == '' and line != '':
                titles.append(line)
            # Subsequent lines of title
            elif previous != '' and line != '':
                titles[-1] += line if line[-1] == '-' else " " + line
            previous = line
    titles = [x.replace('- ', '-') for x in titles if x != '-']
    for t in titles:
        print(str(year) + ": " + t)
        rows.append({'year': year,
                     'congress': congresses[year],
                     'title': t,
                     'abstract': "N/A",
                     'link': link,
                     'tags': ""})
    return rows

def html1997(year):
    link = "https://events.ccc.de/congress/1997/CongressFahrplan.html"
    soup = bs(get(link).text, 'html5lib')
    schedules = soup.findAll('pre')
    titles, abstracts, day = [], [], 0
    for s in schedules:
        day += 1
        s = s.text
        with open("1997day" + str(day) + ".txt", 'w') as f:
            f.write(s)
        with open("1997day" + str(day) + ".txt", 'r') as f:
            lines = f.readlines()
        line = s.split('\n')
        inside = False
        buffer = []
        for line in lines:
            if line[0] == "-":
                # If buffer has the previous abstract, flush buffer
                if buffer:
                    abstract = ""
                    for b in buffer:
                        abstract = abstract + b
                    abstracts.append(abstract.strip())
                    buffer = []
                # Found beginning of titles and abstracts section
                inside = True
                # Found title
                titles.append(line[2:].strip())
                print(str(year) + ": " + titles[-1])
            elif inside:
                # Found a line of abstract, gather it to buffer
                buffer.append(line.strip() + " ")
        # If last abstract is still in buffer, write it
        if buffer:
            abstract = ""
            for b in buffer:
                abstract = abstract + b
            abstracts.append(abstract.strip())
    rows = []
    # Last title in the schedule has no abstract, not even a newline :(
    abstracts.append("")
    for t,a in zip(titles, abstracts):
        rows.append({'year': year,
                     'congress': congresses[year],
                     'title': t,
                     'abstract': a,
                     'link': link,
                     'tags': ""})
    return rows

def html1998(year):
    head = "https://events.ccc.de/congress/" + str(year) + "/"
    tail = "fahrplan.html" if year == 1999 else "fahrplan-beschreibung.html"
    link = head + tail
    # print(link)
    soup = bs(get(link).text, 'html5lib')
    titles = [td.text for td in soup.findAll('td', attrs={'width': '30%'})]
    titles = [x.replace('\n', ' ').strip() for x in titles]
    titles = [x.replace('\t', ' ').strip() for x in titles]
    abstracts = [td.text for td in soup.findAll('td', attrs={'width': '50%'})]
    abstracts = [x.replace('\n', ' ').strip() for x in abstracts]
    abstracts = [x.replace('\t', ' ').strip() for x in abstracts]
    abstracts = [re.sub(' +', ' ', x) for x in abstracts]
    rows = []
    for t, a in zip(titles, abstracts):
        if t != "":
            print(str(year) + ": " + t)
            rows.append({'year': year,
                         'congress': congresses[year],
                         'title': t,
                         'abstract': a,
                         'link': link,
                         'tags': ""})
    return rows

def html1999(year):
    link = "https://events.ccc.de/congress/1999/fahrplan.html"
    # print(link)
    soup = bs(get(link).text, 'html5lib')
    hrs = soup.select('hr')[:-1]
    hrs = [x for x in hrs if x.findNextSibling(name='h4').text !=""]
    titletags = [x.findNextSibling(name='h4') for x in hrs]
    titles = [x.text for x in titletags]
    abstracttags = [x.findNextSibling(name='p').findNextSibling(name='p') for x in titletags]
    abstracts = [re.sub(' +', ' ', x.text) for x in abstracttags]
    rows = []
    for t, a in zip(titles, abstracts):
        if t != "":
            print(str(year) + ": " + t)
            rows.append({'year': year,
                         'congress': congresses[year],
                         'title': t,
                         'abstract': a,
                         'link': link,
                         'tags': ""})
    return rows

def html2000(year):
    return html2000_2001(year)

def html2001(year):
    return html2000_2001(year)

def html2000_2001(year):
    baselink = "https://events.ccc.de/congress/" + str(year) + "/fahrplan/"
    # day = "day_" if year == 2001 else "day_TAG"
    # startlinks = [baselink + day + str(n) + ".en.html" for n in range(1,4)]
    # startlinks.append(baselink + "events.en.html")
    link = "https://events.ccc.de/congress/" + str(year) + "/fahrplan/events.en.html"
    congress = congresses[year]

    def _event(link):
        print(str(year) + ": " + link)
        soup = bs(get(link).text, 'html5lib')
        title = soup.select('h1')[0].text
        try:
            abstract = soup.select('div.content')[0].find('h3').findNextSibling().text
            abstract = re.sub(' +', ' ', abstract.replace('\n',' ').strip())
        except:
            abstract = "N/A"
        print(str(year) + ": " + title)
        return {'year': year,
                'congress': congress,
                'title': title,
                'abstract': abstract,
                'link': link,
                'tags': ""}

    def _meta(link):
        print(str(year) + ": Metaprocessing " + link)
        soup = bs(get(link).text, 'html5lib')
        selector = 'a.internal' if "2000" in baselink else 'a'
        links = [baselink + a['href'] for a in soup.select(selector) if 'event/' in a['href']]
        return [_event(l) for l in links]

    rows = []
    #for l in startlinks:
    #    rows.extend(_meta(l))
    rows.extend(_meta(link))
    return rows

# -= High level functions =-

def _sort(rows):
    """ Sort rows from 1C3 to 32C3 """
    sortedrows = []
    for serial in range(1, 33):
        congressrows = []
        for row in rows:
            if row['congress'].rstrip("C3") == str(serial):
                congressrows.append(row)
        for row in congressrows:
            sortedrows.append(row)
    return sortedrows

def merge():
    rows = []
    years = congresses.keys()
    missingtotal = 0
    for year in years:
        if str(year) in [x.rstrip(".csv") for x in glob("*.csv")]:
            for row in load(str(year) + ".csv"):
                rows.append(row)
        else:
            missingtotal += 1
            print("- Missing CSV for year " + str(year) + " [" + congresses[year] + "]")
    print("= Total missing: " + str(missingtotal))
    new(o)
    # TODO: Check if this is really necessary:
    _sort(rows)
    save(o, rows)
    print("DONE")

def main():
    # list(map(manual, manualyears))
    # list(map(html, htmlyears))
    # list(map(ical, icalyears))
    list(map(xml, xmlyears))
    merge()

if __name__ == "__main__":
        main()
