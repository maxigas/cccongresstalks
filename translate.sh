#!/bin/bash

# OOM killer kills Apertium on 20> kb files (on my laptop with 8GB RAM)

find . -name '????.csv' -exec split --lines=24 -d --additional-suffix=.csv '{}' $(basename '{}')_split \;
rm -rf tmp
mkdir tmp
mv -v *_split??.csv
cd tmp
find . -name '*.csv' -exec apertium deu-eng '{}' $(basename '{}')_in_eng \;
# TODO: cat files
cat 


# rename -v 's/\.csv_in_eng/_in_eng\.csv/' *_in_eng




