#!/bin/bash
# Synonyms preprocessor for the original csv files:
# Reduce similar words to a general term chosen by the researchers.
# The integral sign (∫) is added to the general term to signify that the data was changed.
# Options for rpl

# set -x

# DEBUG:
# O='--whole-words --backup --verbose --case-sensitive'
# O='--whole-words'
O='--ignore-case'

# Integral sign
# I='∫'

# rpl $OPTIONS foo bar$I $1

declare -A SYN; declare -a KEYS

# ATTACKz
SYN[angriffe]=attack; KEYS+=( "angriffe" )
SYN[attacks]=attack; KEYS+=( "attacks" )
SYN[attack]=attackz; KEYS+=( "attack" )

# HACKz
SYN[hacking]=hack; KEYS+=( "hacking" )
SYN[hacker]=hack; KEYS+=( "hacker" )
SYN[hack]=hackz; KEYS+=( "hack" )

# INTRODUCTIONz
SYN[überblick]=Anfänger; KEYS+=( "überblick" )
SYN[ueberblick]=Anfänger; KEYS+=( "ueberblick" )
SYN[einführung]=Anfänger; KEYS+=( "einführung" )
SYN[einfuehrung]=Anfänger; KEYS+=( "einfuehrung" )
SYN[anfängerinnen]=Anfänger; KEYS+=( "anfängerinnen" )
SYN[anfängerinneninnen]=Anfänger; KEYS+=( "anfängerinneninnen" )
SYN[anfängerkompatibel]=Anfänger; KEYS+=( "anfängerkompatibel" )
SYN[beginners]=Anfänger; KEYS+=( "beginners" )
SYN[introduction]=Anfänger; KEYS+=( "introduction" )
SYN[informieren]=Anfänger; KEYS+=( "informieren" )
SYN[anfänger]=Anfängerz; KEYS+=( "anfänger" )

# SECURITYz

SYN[abhören]=security; KEYS+=( "abhören" ) # intercept/tap
SYN[kompromittierende]=security; KEYS+=( "kompromittierende" )
SYN[sicherheit]=security; KEYS+=( "sicherheit" ) # security
SYN[verwundbarkeit]=security; KEYS+=( "verwundbarkeit" ) # vulnerability
SYN[vulnerabilities]=security; KEYS+=( "vulnerabilities" )
SYN[vulnerable]=security; KEYS+=( "vulnerable" )
SYN[exploit]=security; KEYS+=( "exploit" )
SYN[exploiting]=security; KEYS+=( "exploiting" )
SYN[secure]=security; KEYS+=( "secure" )
SYN[security]=securityz; KEYS+=( "security" )

# TERRORz
SYN[terrorismus]=terror; KEYS+=( "terrorismus" )
SYN[terror]=terrorz; KEYS+=( "terror" )

# VIRUSz
SYN[virus]=viren; KEYS+=( "virus" )
SYN[virenforum]=viren; KEYS+=( "virenforum" )
SYN[viren]=virenz; KEYS+=( "viren" )

for i in "${KEYS[@]}"; do
    echo "$i → ${SYN[$i]}"
    rpl $O "$i" "${SYN[$i]}" *.csv
done

# for s in "${!SYN[@]}"
# do
#     echo "${SYN[$s]}$I" ← $s

# done

