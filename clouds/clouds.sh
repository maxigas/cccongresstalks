#!/bin/bash
# Generate individual word clouds for all the csv files

# How to install dependencies on Debian systems?
# Run manually or uncomment the following lines:

# sudo apt install csvkit rpl pip3 rename imagemagick
# sudo pip3 update
# sudo pip3 install wordcloud

set -x

rm *.png index.html
cd ..
mkdir tmp
cp *.csv tmp
cp synonyms.sh tmp
cd tmp

# Take only the title and abstract columns
#### find . -wholename './*.csv' -type f -exec sh -c 'csvcut -d "|" -c 3,4 $0 > $0.out; echo cut $0' {} \;
### ls *.csv | xargs -t -i@ sh -c "csvcut -d '|' -c 3,4 @ > @.out"
parallel -t "csvcut -d '|' -c 3,4 {} > {/}.out" ::: *.csv

# Clean data
#### find . -wholename './*.out' -type f -exec sed -i 's/CLASS:.*\"/\"/g' {} \;
### ls *.out | xargs -t -i@ sed -i 's/CLASS:.*\"/\"/g' @
parallel -t sed -i 's/CLASS:.*\"/\"/g' {} ::: *.out
grep -v -i showcase 2004.csv.out > tmp.csv
mv tmp.csv 2004.csv.out

rename --verbose --force 's|\.csv\.out|\.csv|' *.out

# Unify synonyms to a single term
./synonyms.sh

# Generate word clouds
#### find . -wholename './*.csv' -type f -exec sh -c 'wordcloud_cli --text $0 --imagefile `basename $0 .csv`.png --width 1366 --height 768 --stopwords ../clouds/stopwords.txt; echo cloud $0' {} \;
### ls *.csv | xargs -t -i@ sh -c 'wordcloud_cli --text @ --imagefile $(basename $1 .csv).png --width 1366 --height 768 --stopwords ../clouds/stopwords.txt' - @
parallel -t wordcloud_cli --text {/} --imagefile {/.}.png --width 1366 --height 768 --stopwords ../clouds/stopwords.txt ::: *.csv

# Generate labels
# http://unixetc.co.uk/2014/08/24/picture-frames-and-captions-with-imagemagick/
#### find . -wholename './*.png' -type f -exec sh -c 'montage -verbose -pointsize 100 -label `basename $0 .png` $0 -frame 10 -geometry +0+0 $0.png' {} \;
### ls *.png | xargs -t -i@ sh -c "montage -verbose -pointsize 100 -label $(basename $1 .png) foo -frame 10 -geometry +0+0 foo.png" - @
parallel montage -verbose -pointsize 100 -label {/.} {} -frame 10 -geometry +0+0 {/}.png ::: *.png

# Generate directory listing (index.html)
cat >index.html <<EOF
<html>
<h1 style="font-size: 100pt">Word clouds from Chaos Communication Congress talks' titles and abstracts</h1>

<h2 style="font-size: 75pt"><a href="https://gitlab.com/maxigas/cccongresstalks">Data and background available on GitLab</a></h2>

EOF

for x in {1984..2019}; do
    echo "<a href=\"https://gitlab.com/maxigas/cccongresstalks/raw/master/clouds/$x.png?inline=false\"><img src=\"$x.png\" alt=\"$x\"></a>" >> index.html
done

echo "" >> index.html
echo "</html>" >> index.html

# Move results out and clean up temporary directory
rename --verbose --force 's|\.png\.png|\.png|' *.png.png
mv -v *.png index.html ../clouds
cd ..
# DEBUG:
rm -rf tmp
